package com.qa.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RunA_SQL {
	
	public static String run_a_SQL_Query(String server, String port, String databaseName, 
			String user, String password, String sql, String resultsSet) {
		System.setProperty("java.net.preferIPv6Addresses", "true");

		String nextDeviceIs = "";

		System.setProperty("java.net.preferIPv6Addresses", "true");

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			String connectionUrl =
					"jdbc:sqlserver://"+server+":"+port+";" + "databaseName="+databaseName+";" + "user="+user+";"
							+ "password="+password+";" + "resultsSet="+resultsSet+";";

			ResultSet resultSet = null;

			System.out.println("Connection url =" + connectionUrl);

			try (Connection connection = DriverManager.getConnection(connectionUrl);
					Statement statement = connection.createStatement();) {

				// Create and execute a SELECT SQL statement.
				String selectSql = sql;
				resultSet = statement.executeQuery(selectSql);

				// Print results from select statement
				while (resultSet.next()) {
					System.out.println(resultSet.getString(1));
					nextDeviceIs = resultSet.getString(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			// System.out.println("Deleting user " + email + " from the Core DB");
		} catch (Exception e) {
			System.out.println(e);
		}
		return nextDeviceIs;
	}
}
