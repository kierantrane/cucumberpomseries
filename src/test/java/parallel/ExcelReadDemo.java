package parallel;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.base.BaseTest;
import com.qa.util.Helper;
import com.qa.util.TestUtil;

public class ExcelReadDemo extends BaseTest  {
	
	public static String TESTDATA_SHEET_PATH = "src/testfiles/ArnottsGiftCardReports.xlsx";
	String username = Helper.GetOverRideJenkinsUserName("username2");
	String password = Helper.GetOverRideJenkinsPassW("password2");
	
	@DataProvider
	public Object getData() throws Exception {
		Object testData[][] = TestUtil.getTestData("Type1", TESTDATA_SHEET_PATH);
		return testData;

	}
	
	@Test(priority = 1, dataProvider = "getData")
		public void checkReportsType1() {
	}
	
	

}
