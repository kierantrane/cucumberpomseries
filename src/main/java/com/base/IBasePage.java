package com.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

public interface IBasePage {
	String getPageTitle();

	boolean isElementDisplayed(By locator);

	String getPageHeader(By locator);

	WebElement getElement(By locator);

	void waitForElementPresent(By locator);

	void waitForPageTitle(String title);

	//ExpectedCondition<Object> customeJsReturnsValue(String javaScript);
}
