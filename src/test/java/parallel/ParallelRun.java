//Right click + run as TestNG to see rerun

package parallel;

import org.junit.runner.RunWith;
import org.testng.annotations.DataProvider;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/parallel/LoginPage.feature"}, //LoginPage.feature ContactUs.feature
		glue = {"parallel"}, //, "AppHooks"
		plugin = {"pretty", 
				"json:target/cucumber.json",
				"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
				"timeline:test-output-thread/", //Thread
				"rerun:target/failedrerun.txt" 
				}, 
		tags = "not @Smoke",
		monochrome = true
		)


public class ParallelRun extends AbstractTestNGCucumberTests{	
	
	@Override
	@DataProvider(parallel=true) 
	public Object[][] scenarios() {
		return super.scenarios();
	}
	

}
