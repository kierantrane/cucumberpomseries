////Bavarian INN
////REMOVE
//
//package com.base;
//
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//
//import java.util.Iterator;
//import java.util.List;
//import java.util.Properties;
//import java.util.Random;
//import java.util.concurrent.TimeUnit;
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.firefox.FirefoxBinary;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.support.events.EventFiringWebDriver;
//import org.openqa.selenium.support.ui.FluentWait;
//import org.testng.annotations.AfterMethod;
////import com.bav.uat.pages.CheckBalancePage;
////import com.bav.uat.pages.DetailsPage;
////import com.bav.uat.pages.LocationsPage;
////import com.bav.uat.pages.LoginPage;
//
//import com.google.common.base.Function;
//
//import com.qa.util.Helper;
//import com.qa.util.TestUtil;
//import com.qa.util.WebEventListener;
//
//
//public class OLD_TestBase {
//
//	//WebDriverWait wait = null;
//	FluentWait<WebDriver> wait = null;
//	public static WebDriver driver;
//	public static Properties prop;
//	public static EventFiringWebDriver e_driver;
//	public static WebEventListener eventListener;
//	static String userDir = System.getProperty("user.dir");
//
////	LoginPage loginPage;
////	LocationsPage locationsPage;
////	DetailsPage detailsPage;
////	CheckBalancePage checkPageBalance;
//
//	static String browserName = "";
//
//	HttpURLConnection huc = null;
//	int respCode = 200;
//	
//
//	public OLD_TestBase() {
//		
//		try {
//			
//			prop = new Properties();
//			FileInputStream ip = new FileInputStream(
//					System.getProperty("user.dir") + "/src/main/java/com/" + "/qa/config/config.properties");
//			prop.load(ip);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		 
//		
//	} // End TestBase
//	
//	
//	public static long getRandom9DigitPhoneNumberString() {
//		long theRandomNum = (long) (Math.random()*Math.pow(9,9));
//		return theRandomNum;
//		}
//
//
//	public static void initialization() {
//
//		browserName = Helper.GetOverRideJenkinsBrowser("browser");
//		String testURL = Helper.GetOverRideJenkinsURL("url");
//		//String downloadSpeed = Helper.GetOverRideJenkinsDownloadSpeed("downloadSpeed");
//		//String uploadSpeed = Helper.GetOverRideJenkinsURL("uploadSpeed");
//
//		System.out.println("System Browser selects is " + browserName);
//		// Chrome
//		if (browserName.equals("chrome")) {
//			System.setProperty("webdriver.chrome.driver", userDir + "/drivers/chromedriver.exe");
//			driver = new ChromeDriver();
//
//			// Chrome headless
//		} else if (browserName.equals("chrome_headless")) {
//			System.setProperty("webdriver.chrome.driver", userDir + "/drivers/chromedriver.exe");
//			ChromeOptions options = new ChromeOptions(); // Added
//			options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
//					"--ignore-certificate-errors"); // Added
//			driver = new ChromeDriver(options); // Added
//		} else
//
//		// Firefox headless
//		if (browserName.equals("firefox_headless")) {
//
//			FirefoxBinary firefoxBinary = new FirefoxBinary();
//			firefoxBinary.addCommandLineOptions("--headless");
//
//			System.setProperty("webdriver.gecko.driver", userDir + "/drivers/geckodriver.exe");
//			FirefoxOptions firefoxOptions = new FirefoxOptions();
//			firefoxOptions.setBinary(firefoxBinary);
//			driver = new FirefoxDriver(firefoxOptions);
//
//			// Firefox
//		} else if (browserName.equals("firefox")) {
//			System.setProperty("webdriver.gecko.driver", userDir + "/drivers/geckodriver.exe");
//			driver = new FirefoxDriver();
//		}
//
//		e_driver = new EventFiringWebDriver(driver);
//		// Now create object of EventListerHandler to register it with
//		// EventFiringWebDriver
//		eventListener = new WebEventListener();
//		e_driver.register(eventListener);
//		driver = e_driver;
//
//		driver.manage().window().maximize();
//		driver.manage().deleteAllCookies();
//		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
//		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
//
////		int downloadSpeedInt = Integer.valueOf(downloadSpeed);
////		int uploadSpeedInt = Integer.valueOf(downloadSpeed);
////		// Insert download speed here
////		if (downloadSpeedInt > 0 && uploadSpeedInt > 0) {
////
////		}
//
//		//
//
//		driver.get(testURL);
//
//	} // End initialization
//
//	public boolean checkLinks() {
//		String url = Helper.GetOverRideJenkinsURL("url");
//		// String url = prop.getProperty("url");
//
//		String homePage = Helper.GetOverRideJenkinsURL("url");
//		// String homePage = prop.getProperty("url");
//
//		List<WebElement> links = driver.findElements(By.tagName("a"));
//		Iterator<WebElement> it = links.iterator();
//		while (it.hasNext()) {
//			url = it.next().getAttribute("href");
//			System.out.println(url);
//
//			if (url == null || url.isEmpty()) {
//				System.out.println("URL is either not configured for anchor tag or it is empty");
//				continue;
//			}
//
//			if (!url.startsWith(homePage)) {
//				System.out.println("URL belongs to another domain, skipping it.");
//				continue;
//			}
//
//			try {
//				huc = (HttpURLConnection) (new URL(url).openConnection());
//				huc.setRequestMethod("HEAD");
//				huc.connect();
//				respCode = huc.getResponseCode();
//				if (respCode >= 400) {
//					System.out.println(url + " is a broken link");
//					return false;
//				} else {
//					System.out.println(url + " is a valid link");
//
//				}
//
//			} catch (MalformedURLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} // End while
//
//		System.out.println(" ***********  End of link checks for this page ***************** ");
//
//		return true;
//	}
//
//	public static void clickElementByJS(WebElement element, WebDriver driver) {
//		JavascriptExecutor js = ((JavascriptExecutor) driver);
//		js.executeScript("arguments[0].click();", element);
//	}
//
//	// Depricated
////	public void waitForPageTitle(String pageTitle) {
////		try {
////			wait.until(ExpectedConditions.titleContains(pageTitle));
////		} catch (Exception e) {
////			System.out.println("Some excpeption/error occured waiting for the element " + pageTitle.toString());
////			e.printStackTrace();
////		}
////
////	}
//
//	public void waitForPageTitle(String waitForTitle) {
//
//		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
//			public WebElement apply(WebDriver driver) {
//				return driver.findElement(By.xpath("//title"));
//			}
//
//		});// End new Function
//
//		System.out.println("Text searched for '" + waitForTitle + "', displayed =" + element.isDisplayed());
//	}
//
//	public void waitForElementPresent(By locator) {
//
//		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
//			public WebElement apply(WebDriver driver) {
//				return driver.findElement(locator);
//			}
//
//		});// End new Function
//
//		System.out.println("Locator '" + locator + "' displayed =" + element.isDisplayed());
//	}
//	
//
//	// Depricated
////	public void waitForElementPresent(WebElement locator) {
////		try {
////			wait.until(ExpectedConditions.presenceOfElementLocated((By) locator));
////		} catch (Exception e) {
////			System.out.println("Some excpeption/error occured waiting for the element " + locator.toString());
////			e.printStackTrace();
////		}
////
////	}
//
//	public boolean isTextDisplayed(String string) {
//		WebElement searchName = driver.findElement(By.xpath("//*[contains(text(), '" + string + "')]"));
//		if (searchName.isDisplayed()) {
//			return true;
//		}
//		return false;
//	}
//
//
//	public static boolean waitPageLoadedByDriver(WebDriver driver) {
//		return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("loaded")
//				|| ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
//	}
//
//	@AfterMethod
//	public void tearDown() throws IOException {
//		driver.close();
//		if (!browserName.contains("firefox")) {
//			driver.quit();
//		}
//	}
//
//
//}
