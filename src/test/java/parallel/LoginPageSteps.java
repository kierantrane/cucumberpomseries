package parallel;

import org.junit.Assert;

import com.pages.LoginPage;
import com.qa.factory.DriverFactory;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginPageSteps {

	private LoginPage loginPage = new LoginPage(DriverFactory.getDriver());
	private static String title;
	
	String url = "http://www.automationpractice.com/index.php?controller=authentication&back=my-account";
	//String url = "https://www.rte.ie/";

	@Given("user is on login page")
	public void user_is_on_login_page() {
		System.out.println("Go to the url ="+url);
		DriverFactory.getDriver().get(url);
	}

	@When("user gets the title of the page")
	public void user_gets_the_title_of_the_page() {
		title = loginPage.getLoginPageTitle();
		System.out.println("Page title is =" + title);
	}

	@Then("page title should be {string}")
	public void page_title_should_be(String expectedTitleName) {
		Assert.assertTrue(title.contains(expectedTitleName));
	}

	//Assertions in the Then
	@Then("the forgot your password link should be displayed")
	public void the_forgot_your_password_link_should_be_displayed() {
		boolean isDisplayed = loginPage.isForgotPwdLinkExists();
		Assert.assertTrue(isDisplayed);
	}

	@When("the user enters username {string}")
	public void the_user_enters_username(String userName) {
		loginPage.enterUserName(userName);
	}

	@When("the user enters password {string}")
	public void the_user_enters_password(String passWord) {
		loginPage.enterPassword(passWord);
	}

	@When("user selects login button")
	public void user_selects_login_button() {
		loginPage.clickOnLogin();

	}

	
}
