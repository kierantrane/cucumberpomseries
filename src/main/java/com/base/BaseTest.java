// Catalyst base 
package com.base;

import org.testng.annotations.AfterMethod;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang.LocaleUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.*;
/*import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;*/
import org.openqa.selenium.devtools.v96.emulation.Emulation;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

import com.qa.util.Helper;
import com.qa.util.TestUtil;
//import com.qa.util.WebEventListener;

import io.codearte.jfairy.Fairy;
import io.github.bonigarcia.wdm.WebDriverManager;


public class BaseTest {

	public static WebDriver driver;
	public static BasePage page;
	public static WebDriverWait wait;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	//public static WebEventListener eventListener;
	static String userDir = System.getProperty("user.dir");
	public Fairy fairy = null;
	

	public ThreadLocal<WebDriver> tdriver = new ThreadLocal<WebDriver>();

	HttpURLConnection huc = null;
	int respCode = 200;
	static String browserName;

	protected Fairy GetLocalFairy() {
		return this.fairy;
	}

	public BaseTest() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "/src/main/java/com/" + "/qa/config/config.properties");
			prop.load(ip);
			String locale = Helper.GetOverRideJenkinsLocale("locale");

			// Converts String to type Locale
			Locale createLocale = LocaleUtils.toLocale(locale);
			fairy = Fairy.create(createLocale);// first create Fairy object. By default - Locale is English

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} // End TestBase
	
	public static void initialization() throws IOException, SocketException {
		
		browserName = Helper.GetOverRideJenkinsBrowser("browser");
		String testURL = Helper.GetOverRideJenkinsURL("url2");
		String downloadSpeed = Helper.GetOverRideJenkinsDownloadSpeed("downloadSpeed");
		String uploadSpeed = Helper.GetOverRideJenkinsURL("uploadSpeed");
		
		String longitdue1 = Helper.GetOverRideJenkinsPassW("longitdue");
		 String latitude1 = Helper.GetOverRideJenkinsPassW("latitude");
		 String accuracy1 = Helper.GetOverRideJenkinsPassW("accuracy");
		// String geoUrl = Helper.GetOverRideJenkinsPassW("geoUrl");
		 double longitdue = Double.valueOf(longitdue1);
		 double latitude = Double.valueOf(latitude1);
		 double accuracy = Double.valueOf(accuracy1);
		 
		 

		// Chrome
		if (browserName.equals("chrome")) {
			// System.setProperty("webdriver.chrome.driver", userDir +
			// "/drivers/chromedriver.exe"); //old
			WebDriverManager.chromedriver().setup();
			
			driver = new ChromeDriver();
//			DevTools devTools = ((HasDevTools) driver).getDevTools();
//			devTools.createSession();
//			devTools.send(Emulation.setGeolocationOverride(Optional.of(longitdue),
//															Optional.of(latitude),
//			                                               Optional.of(accuracy)));

			// Chrome headless
		} else if (browserName.equals("chrome_headless")) {

			// System.setProperty("webdriver.chrome.driver", userDir +
			// "/drivers/chromedriver.exe"); //old
			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions(); // Added
			options.addArguments("--disable-extensions"); // Added
			options.addArguments("--dns-prefetch-disable"); // Added
			options.addArguments("--disable-gpu"); // Added
			options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
					"--ignore-certificate-errors"); // Added
			options.addArguments("--no-sandbox"); // Added 10/02/2020
			driver = new ChromeDriver(options); // Added
			
			//DevTools devTools = ((HasDevTools) driver).getDevTools();
			//devTools.createSession();
			//devTools.send(Emulation.setGeolocationOverride(Optional.of(longitdue),
															//Optional.of(latitude),
			                                              // Optional.of(accuracy)));

		} else

		// Firefox headless
		/*if (browserName.equals("firefox_headless")) {
			FirefoxBinary firefoxBinary = new FirefoxBinary();
			firefoxBinary.addCommandLineOptions("--headless");

			System.setProperty("webdriver.gecko.driver", userDir + "/drivers/geckodriver.exe");
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			firefoxOptions.setBinary(firefoxBinary);
			driver = new FirefoxDriver(firefoxOptions);

			// Firefox
		} else if (browserName.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", userDir + "/drivers/geckodriver.exe");
			driver = new FirefoxDriver();

			System.out.println("Starting Firefox");

			// Commented this 4 lines out to make set the network speed
			e_driver = new EventFiringWebDriver(driver);
			eventListener = new WebEventListener();
			e_driver.register(eventListener);
			driver = e_driver;
		} else */ 
//			if (browserName.equals("opera")) {
//			System.setProperty("webdriver.opera.driver", userDir + "/drivers/operadriver.exe");
//
//			driver = new OperaDriver();
//			System.out.println("Starting Opera");
//
//			// Commented this 4 lines out to make set the network speed
//			e_driver = new EventFiringWebDriver(driver);
//			eventListener = new WebEventListener();
//			e_driver.register(eventListener);
//			driver = e_driver;
//		} else if (browserName.equals("msedge")) {
//			System.setProperty("webdriver.edge.driver", userDir + "/drivers/msedgedriver.exe");
//			driver = new EdgeDriver();
//			System.out.println("Starting Ms Edge");
//
//			// Commented this 4 lines out to make set the network speed
//			e_driver = new EventFiringWebDriver(driver);
//			eventListener = new WebEventListener();
//			e_driver.register(eventListener);
//			driver = e_driver;
//		}

		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver

//Commented this 4 lines out to make set the network speed
		// e_driver = new EventFiringWebDriver(driver);
		// eventListener = new WebEventListener();
		// e_driver.register(eventListener);
		// driver = e_driver;

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		// Code for Network Throttling
		int downloadSpeedInt = Integer.valueOf(downloadSpeed);
		int uploadSpeedInt = Integer.valueOf(uploadSpeed);
		System.out.println("downloadSpeedInt = " + downloadSpeedInt);
		System.out.println("uploadSpeedInt = " + uploadSpeedInt);
		// Insert download speed here

//		if (downloadSpeedInt > 0 && uploadSpeedInt > 0) {
//			CommandExecutor executor = ((RemoteWebDriver) driver).getCommandExecutor();
//			Map<String, Comparable> map = new HashMap<String, Comparable>();
//			map.put("offline", false);
//			map.put("latency", 5);
//			map.put("download_throughput", downloadSpeedInt);
//			map.put("upload_throughput", uploadSpeedInt);
//			//Response response = executor.execute(new Command("setNetworkConditions", ImmutableMap.of("network_conditions", ImmutableMap.copyOf(map))));
//		}

		try {
			driver.get(testURL);
		} catch (Exception e) {
			System.out.println("Exception caught launching site = " + testURL + ": " + e.getMessage());
		}

		page = new BasePage(driver);

	} // End initialization

@BeforeMethod
public void setUpTest() throws Exception {
	initialization();

}

@AfterMethod(alwaysRun = true)
public void tearDown() throws IOException {
	
	try {
		System.out.println("Closing browser");
		driver.close();	

		if (!browserName.contains("firefox")) {
			driver.quit();
		}
		
	}
	catch (Exception e) {
		System.out.println("Exception caught is ="+e.getMessage());
	}
	
	
	
}

	public static String whatsTodaysDateSOAP() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String todaysDate = formatter.format(date);

		return todaysDate;

	}

	public static String whatsTodaysDateAmericanSlashes() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String todaysDate = formatter.format(date);

		return todaysDate;

	}

	public static String whatsTodaysDateAmerican() {
		SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
		Date date = new Date();
		String todaysDate = formatter.format(date);

		return todaysDate;

	}

	public static String whatsTodaysDateEpos() {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		Date date = new Date();
		String todaysDate = formatter.format(date);

		return todaysDate;

	}

	public static String whatsTodaysDateAndTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

		Date date = new Date();
		String todaysDate = formatter.format(date);

		return todaysDate;

	}

	public static synchronized WebDriver getDriver() {
		return driver;
	}

	public boolean searchOnPageForData(String searchForThis) throws Exception {
		System.out.println("Searching for text " + searchForThis);
		boolean isDisplayed = false;
		System.out.println("Start of while");
		Thread.sleep(1000);

		if (driver.getPageSource().contains(searchForThis)) {
			isDisplayed = true;

		} else

		{
			isDisplayed = false;
			System.out.println("Search for the string '" + searchForThis + "' is = " + isDisplayed);
			System.out.println("Search for text next not found");
		}

		System.out.println("Search for the string '" + searchForThis + "' is = " + isDisplayed);
		return isDisplayed;

	} // End while

		

	public boolean checkIfElementIsDisplayed(String xpath) {
		List<WebElement> dynamicElement = driver.findElements(By.xpath(xpath));
		if (dynamicElement.size() != 0) {
			// If list size is non-zero, element is present
			System.out.println("Element present = " + xpath);
			return true;
		} else {
			// Else if size is 0, then element is not present
			System.out.println("Element not present =" + xpath);
		}

		return false;

	}

	public boolean checkLinks() {
		String url = Helper.GetOverRideJenkinsURL("url2");
		// String url = prop.getProperty("url");

		String homePage = Helper.GetOverRideJenkinsURL("url2");
		// String homePage = prop.getProperty("url");

		List<WebElement> links = driver.findElements(By.tagName("a"));
		Iterator<WebElement> it = links.iterator();
		while (it.hasNext()) {
			url = it.next().getAttribute("href");
			System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("URL is either not configured for anchor tag or it is empty");
				continue;
			}

			if (!url.startsWith(homePage)) {
				System.out.println("URL belongs to another domain, skipping it.");
				continue;
			}

			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());
				huc.setRequestMethod("HEAD");
				huc.connect();
				respCode = huc.getResponseCode();
				if (respCode >= 400) {
					System.out.println(url + " is a broken link");
					System.out.println("Broken url code = " + respCode);
					return false;
				} else {
					System.out.println(url + " is a valid link");

				}

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} // End while

		System.out.println(" ***********  End of link checks for this page ***************** ");

		return true;
	}

	public String getTime() {

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date date = new Date();
		String time = formatter.format(date);
		System.out.println(formatter.format(date));

		return time;
	}

}

//By blueBOrdersNext = By
//.xpath("//*[@id='myOrder']/app-snipp-rewards-orders/div/div/div/div/mat-paginator/div/div/div/button[2])");

//public boolean searchBBPortalOrder(String searchForThis) throws Exception {
//System.out.println("Search for text '" + searchForThis + "'");
//boolean isDisplayed = false;
//
//while (page.getInstance(MyOrdersPage.class).isElementDisplayed(By.xpath(
//		"//*[@id='myOrder']/app-snipp-rewards-orders/div/div/div/div/mat-paginator/div/div/div/button[2]")))
//	;
//{
//	System.out.println("Item is displayed");
//	page.getInstance(MemberViewMember_InfoPage.class).click(By.xpath(
//			"//*[@id='myOrder']/app-snipp-rewards-orders/div/div/div/div/mat-paginator/div/div/div/button[2]"));
//	System.out.println("Start of while");
//	Thread.sleep(3000);
//	isDisplayed = page.getInstance(MemberViewMember_InfoPage.class).isTextDisplayed(searchForThis);
//
//	System.out.println("Search for the string '" + searchForThis + "' is = " + isDisplayed);
//
//	if (isDisplayed = true) {
//		System.out.println("if true");
//		System.out.println("Found the text = " + searchForThis);
//		// break;
//	}
//
//} // End while
//return isDisplayed;
//}

//public boolean searchAuditLogs2(String searchForThis) throws Exception {
//System.out.println("Searching for text " + searchForThis);
//boolean isDisplayed = false;
//// boolean nextDisp = false;
//
//while (page.getInstance(MemberViewMember_InfoPage.class).isElementDisplayed(By.partialLinkText("Next"))) {
//
//	System.out.println("Start of while");
//	Thread.sleep(1000);
//
//	if (driver.getPageSource().contains(searchForThis)) {
//		isDisplayed = true;
//		break;
//	} else if (page.getInstance(MemberViewMember_InfoPage.class).isLinkClickable(By.partialLinkText("Next")))
//		;
//	{
//		page.getInstance(MemberViewMember_InfoPage.class).click(By.partialLinkText("Next"));
//		System.out.println("Search for the string '" + searchForThis + "' is = " + isDisplayed);
//	}
//	System.out.println("Search for text next not found");
//	isDisplayed = false;
//	System.out.println("Search for the string '" + searchForThis + "' is = " + isDisplayed);
//
//	break;
//
//} // End while
//
//return isDisplayed;
//
//} // End searchAuditLogs2

//public String getUrlOfApplication2() throws UnknownHostException, MalformedURLException {
//String testURL = driver.getCurrentUrl();
//System.out.println("Current url =" + testURL);
//String ip = "xx";
//
//try {
//	InetAddress ia = InetAddress.getByName(new URL(testURL).getHost());
//	//InetAddress ia = InetAddress.getByName(testURL);
//	System.out.println("internet address = " + ia.toString());
//	ip = ia.getCanonicalHostName();
//	System.out.println("ip = " + ip.toString());
//} catch (UnknownHostException e) {
//
//}
//
//return ip;
//
//}
//
//public String getUrlOfApplication() throws UnknownHostException {
//String testURL = Helper.GetOverRideJenkinsURL("url2");
//
//HttpServletRequest request = null;
//String ipAddress = request.getHeader("X-FORWARDED-FOR");
//
//if (ipAddress == null) {
//	ipAddress = request.getRemoteAddr();
//}
//// The URL for which IP address needs to be fetched
//InetAddress ip = null;
//try {
//	// Fetch IP address by getByName()
//	ip = InetAddress.getByName(new URL(testURL).getHost());
//
//	// Print the IP address
//	System.out.println("Public IP Address of: " + ip.toString());
//} catch (MalformedURLException e) {
//	// It means the URL is invalid
//	System.out.println("Invalid URL");
//}
//
//return ip.toString();
//}