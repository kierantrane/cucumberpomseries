package parallel;

import org.junit.runner.RunWith;
import org.testng.annotations.DataProvider;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"@target/failedRerun.txt"}, //LoginPage.feature ContactUs.feature
		glue = {"parallel"}, //, "AppHooks"
		plugin = {"pretty", 
				"json:target/cucumber.json",
				"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
				"timeline:test-output-thread/", //Thread
				"rerun:target/failedrerun.txt" 
				}, 
		monochrome = true
		)
public class FailedRun extends AbstractTestNGCucumberTests{
	
	@Override
	@DataProvider(parallel=true) 
	public Object[][] scenarios() {
		return super.scenarios();
	}
	

}
