package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	private WebDriver driver;
	
	//1, By Locators
	private By emailid = By.id("email");
	private By password = By.id("passwd");
	private By signin = By.id("SubmitLogin");
	
	private By forgotPasswordLink = By.linkText("Forgot your password?");
	
	//2, Constructor of page class
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	//3, Page Actions :  features(behavour) of the page of the form of methods
	public String getLoginPageTitle() {
		return driver.getTitle();
	}
	
	public boolean isForgotPwdLinkExists() {
		return driver.findElement(forgotPasswordLink).isDisplayed();
	}
	
	public void enterUserName(String username) {
		driver.findElement(emailid).sendKeys(username);
	}
	public void enterPassword(String passWord) {
		driver.findElement(password).sendKeys(passWord);
	}
	
	public void clickOnLogin() {
		driver.findElement(signin).click();
	}
	
}
