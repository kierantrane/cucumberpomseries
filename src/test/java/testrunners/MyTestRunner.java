package testrunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/AppFeatures"}, //LoginPage.feature
		glue = {"stepsdefinations", "AppHooks"},
		plugin = {"pretty", 
				"json:target/cucumber.json",
				"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
				"timeline:test-output-thread/" //Thread 
		}, 
		monochrome = true
		)


public class MyTestRunner {

}
