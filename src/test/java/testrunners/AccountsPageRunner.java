package testrunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/AppFeatures/AccountsPage.feature"}, //AccountsPage.feature
		glue = {"stepsdefinations", "AppHooks"},
		plugin = {"pretty"},
		monochrome = true
		)


public class AccountsPageRunner {

}
