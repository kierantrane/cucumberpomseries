Feature: Account Page Feature

#Executed before all the scenarios
Background:
Given user is already logged into the application
|username | password |
| kieran@kieran.com | password1 |


@accounts
Scenario: Accounts page title
Given user is on the Accounts page
When user gets the title of the page
Then page title is "My account - My Store"

@accounts
Scenario: Accounts secion subItems
Given user is on the Accounts page
Then user gets accouts section
|Order history and details|
|Credit slips|
|My addresses|
|My personal information|
|My wishlists |
|Home |
And accounts section count should be 6


