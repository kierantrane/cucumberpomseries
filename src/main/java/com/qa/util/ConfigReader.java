package com.qa.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
	
	private Properties prop;
	
	// Reads properties and returns the Properties object 
	
	public Properties init_prop()  {
		prop = new Properties();
		try {
			FileInputStream ip = new FileInputStream("./src/test/resources/config/config.properties");
			prop.load(ip);
			
		} catch (FileNotFoundException e) {
			System.out.println("Config file not found");
			System.out.println("IOException ="+e.getMessage());
			
		} catch (IOException e) {
			System.out.println("IOException ="+e.getMessage());
			e.printStackTrace();
		}
		
		return prop;
	}

}
