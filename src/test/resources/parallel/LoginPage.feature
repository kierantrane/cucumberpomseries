@Login
Feature: Login page feature

Scenario: Login page feature
Given user is on login page
When user gets the title of the page
Then page title should be "Login - My Store"

Scenario: Forgot Password link
Given user is on login page
Then the forgot your password link should be displayed

#@Regression  @skip_scenario
Scenario: Login with correct credentials
Given user is on login page
When the user enters username "kieran@kieran.com"
And the user enters password "password1"
And user selects login button
Then user gets the title of the page
And page title should be "My account - My Store"
