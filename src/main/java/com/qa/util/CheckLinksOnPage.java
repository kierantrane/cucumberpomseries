//Java streams approach to checking broken links fast

package com.qa.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CheckLinksOnPage {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://amazon.com");

		// @SuppressWarnings("unchecked")
		List<WebElement> links = driver.findElements(By.tagName("a"));
		System.out.println("Number of links are =" + links.size());
		List<String> urlList = new ArrayList<String>();

		for (WebElement e : links) {
			String url = e.getAttribute("href");
			urlList.add(url);

			// checkBrokenLink(url);
		}

		urlList.parallelStream().forEach(e -> checkBrokenLink(e));

		driver.quit();
		System.out.println("Quit here");

	} // End Main

	public static void checkBrokenLink(String linkUrl) {

		
		try {
			URL url = new URL(linkUrl);

			HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
			httpUrlConnection.setConnectTimeout(5000); // set timeout to 5 seconds
			httpUrlConnection.connect();

			System.out.println("Check this url --->=" + linkUrl);

			if (httpUrlConnection.getResponseCode() >= 400) {
				System.out.println(linkUrl + " --> Broken ->" + httpUrlConnection.getResponseMessage());

			} else {
				System.out.println(linkUrl + " --> Good ->" + httpUrlConnection.getResponseMessage());

			}

		} catch (Exception e) {

		}

	} // End checkBrokenLink

}
