package parallel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pages.ContactUsPage;
//import com.base.BasePage;
import com.qa.factory.DriverFactory;
import com.qa.util.BDD_ExcelReader;
import com.qa.util.TestUtil;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ContactUsSteps {
	private ContactUsPage contactUsPage = new ContactUsPage(DriverFactory.getDriver());
	public static String TESTDATA_SHEET_PATH = "src\\files\\automation.xlsx";  //C:\\04Automation\\BitBucket\\CucumberPOMSeries\\
	

	@Given("user navigates to contact us page")
	public void user_navigates_to_contact_us_page() throws Exception {
	   System.out.println("user_navigates_to_contact_us_page");
	   DriverFactory.getDriver().get("http://automationpractice.com/index.php?controller=contact");
	}

	
//	public ArrayList getData() throws Exception {
//		ArrayList testData = TestUtil.getTestDataArray("contactus2", TESTDATA_SHEET_PATH);
//		return testData;
//
//	}
	
	//@Test(dataProvider = "getData")
	@When("user fills the form from the given SheetName {string} and RowNumber {int}")
	 public void user_fills_the_form_from_the_given_sheet_name_and_row_number(String SheetName, Integer rowNumber) throws Exception {
		 
		System.out.println(" *******************************************");
		System.out.println("user fills_the_form from_the_ given_sheet_ name_and_row_number ="+SheetName+" and "+rowNumber);
		
		 BDD_ExcelReader readExcel = new BDD_ExcelReader();
		 List <Map<String, String >> testData = readExcel.getData(TESTDATA_SHEET_PATH, SheetName);
		 
		 String heading = testData.get(rowNumber).get("subjectheading");
		 String email = testData.get(rowNumber).get("email"); 		
		 String orderref = testData.get(rowNumber).get("orderref"); 
		 String message = testData.get(rowNumber).get("message"); 
		 
		 System.out.println("Data read in is "+heading+","+email+","+orderref+","+message);
		 
		 contactUsPage.fillOutContactUsForm(heading, email, orderref, message);
	}

	


	@When("user clicks on the send button")
	public void user_clicks_on_the_send_button() {
		   System.out.println("user_clicks_on_the_send_button");
		   contactUsPage.clickSend();
	}

	@Then("it shows a successful message {string}")
	public void it_shows_a_successful_message(String expectedMessage) {
		String isDisplayed = contactUsPage.getSuccessMsg();
		Assert.assertEquals(isDisplayed, expectedMessage);
	}

}
