Feature: Contact us feature
Scenario Outline: Contact Us scenario with differnt set of data
Given user navigates to contact us page
When user fills the form from the given SheetName "<SheetName>" and RowNumber <RowNumber>
And user clicks on the send button
Then it shows a successful message "Your message has been successfully sent to our team."

Examples:
|SheetName |RowNumber|
|contactus|0|
|contactus|1|
|contactus|2|
|contactus|3|