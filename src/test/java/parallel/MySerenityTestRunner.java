//Right click + run as TestNG to see rerun
package parallel;

import org.junit.runner.RunWith;
import org.testng.annotations.DataProvider;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
//import io.cucumber.testng.AbstractTestNGCucumberTests;
//import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.cucumber.CucumberWithSerenity;


//@RunWith(Cucumber.class)  //Works

@RunWith(CucumberWithSerenity.class) 
@CucumberOptions(
		plugin = {"pretty"},
		glue = {"parallel"}, //, "AppHooks"
		features = {"src/test/resources/parallel/LoginPage.feature"} // ContactUs.feature
		
		
		)


public class MySerenityTestRunner { //extends AbstractTestNGCucumberTests{	
	

	}
	


