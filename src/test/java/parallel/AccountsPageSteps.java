package parallel;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class AccountsPageSteps {

	@Given("user is already logged into the application")
	public void user_is_already_logged_into_the_application(io.cucumber.datatable.DataTable dataTable) {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
		// Map<K, List<V>>. E,K,V must be a String, Integer, Float,
		// Double, Byte, Short, Long, BigInteger or BigDecimal.
		//
		// For other transformations you can register a DataTableType.
		System.out.println("user is already logged into the application");

	}

	@Given("user is on the Accounts page")
	public void user_is_on_the_accounts_page() {
		System.out.println("");
	}

	@Then("page title is {string}")
	public void page_title_is(String string) {
		System.out.println("user is on the Accounts page");
	}

	@Then("user gets accouts section")
	public void user_gets_accouts_section(io.cucumber.datatable.DataTable dataTable) {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
		// Map<K, List<V>>. E,K,V must be a String, Integer, Float,
		// Double, Byte, Short, Long, BigInteger or BigDecimal.
		//
		// For other transformations you can register a DataTableType.
		System.out.println("user gets accouts section");
	}

	@Then("accounts section count should be {int}")
	public void accounts_section_count_should_be(Integer int1) {
		System.out.println("accounts section count should be {int}");
	}

}
