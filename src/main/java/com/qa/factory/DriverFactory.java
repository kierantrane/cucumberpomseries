package com.qa.factory;

import java.util.Optional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v96.emulation.Emulation;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {

	public WebDriver driver;

	public static ThreadLocal<WebDriver> tlLocal = new ThreadLocal<>();

	//Two important things coming from ThreadLocal
	// 1, Get driver
	// 2, Set driver
	// 3, Multiple instance of driver can be accessed for multi threading
	
	
	//Setting up the driver
	public WebDriver init_driver(String browser) {
		browser = browser.trim();
		System.out.println("Browser value = " + browser);

		if (browser.equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			tlLocal.set(new ChromeDriver());
		} else
			if (browser.equals("chrome_headless")) {
				WebDriverManager.chromedriver().setup();
//				tlLocal.set(new ChromeDriver());
				
				ChromeOptions options = new ChromeOptions(); // Added
				options.addArguments("--disable-extensions"); // Added
				options.addArguments("--dns-prefetch-disable"); // Added
				options.addArguments("--disable-gpu"); // Added
				options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
						"--ignore-certificate-errors"); // Added
				options.addArguments("--no-sandbox"); // Added 10/02/2020
				tlLocal.set(new ChromeDriver(options));
				//driver = new ChromeDriver(options); // Added
			} else

//		if (browser.equals("firefox")) {
//			WebDriverManager.firefoxdriver().setup();
//			tlLocal.set(new FirefoxDriver());
//		} else 
			{
			System.out.println("Please pass the correct browser Chrome or Firefox ");
		}
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		
		return getDriver();

	} // End init_driver
	
	//Get the driver with Threadloal
	public static synchronized WebDriver getDriver() { //
		return tlLocal.get();
	}

}
