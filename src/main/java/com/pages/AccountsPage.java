package com.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AccountsPage {
	
	private WebDriver driver;
	
	//1, Constructor of the page class	
	public AccountsPage(WebDriver driver) {
		this.driver = driver;
	}
	
	
	//2, By locators
	private By accountsSetion = By.cssSelector("div#center_column span");
	
	
	
	//3, Page actions
	public int getAccountSectionCount() {
		int accouthCount = driver.findElements(accountsSetion).size();
		return accouthCount;
	}
	
	
	public int getAccountSectionListSize() {
		List <WebElement> accountList = driver.findElements(accountsSetion);
		int size = accountList.size();
		
		return size;
	}
	
	public List <String>getAccountSectionListNames() {
		List<String> accountsList = new ArrayList<>();
		List <WebElement> accountListNames = driver.findElements(accountsSetion);
		
		for(WebElement e : accountListNames) {
			String text = e.getText();
			System.out.println("Adding text "+text +"to the String list");
			accountsList.add(text);
		}
		return accountsList;
		
	}
	
	public String getPageTitle() {
		String titleOfPage = driver.getTitle();
		return titleOfPage;
	}

}
