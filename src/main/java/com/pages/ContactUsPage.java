package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.base.BasePage;

public class ContactUsPage extends BasePage{
	
	private WebDriver driver;
	private By subjectHeading = By.id("id_contact");
	private By email = By.xpath("//*[@id='email' and @name='from']");
	private By orderRef = By.xpath("//*[@id='id_order' and @name='id_order']");
	private By messageText = By.id("message");
	private By sendButton = By.id("submitMessage");
	private By successMsg = By.cssSelector("div#center_column p");
	
	
	//Constructor
	public ContactUsPage(WebDriver driver) {
		this.driver = driver;
	}
	
	//Get Elements
	public WebElement subjectHeading() {
		return getElement(subjectHeading); 
	}
	public WebElement emailAdd() {
		return getElement(email); 
	}
	public WebElement id_order() {
		return getElement(orderRef); 
	}
	public WebElement messageText() {
		return getElement(messageText); 
	}
//	public WebElement sendButton() {
//		return getElement(sendButton); 
//	}
	
//	public WebElement successMsg() {
//		return getElement(successMsg); 
//	}
	
	//Methods
	public void fillOutContactUsForm(String heading, String emailAdd, String orderref, String message) throws InterruptedException {
		System.out.println("Filling out form");
		Select select = new Select(driver.findElement(subjectHeading));
		select.selectByVisibleText(heading);
		
		driver.findElement(email).sendKeys(emailAdd);
		driver.findElement(orderRef).sendKeys(orderref);
		driver.findElement(messageText).sendKeys(message);
//		id_order().sendKeys(orderref);
//		messageText().sendKeys(message);
//		emailAdd().click();
//		emailAdd().sendKeys(emailAdd);
		
		Thread.sleep(2000);
		
	}
	
	public void clickSend() {
		driver.findElement(sendButton).click();
	}
	
	public String getSuccessMsg() {
		return driver.findElement(successMsg).getText();
	}
	
	
}
